#! /usr/bin/env bash
rm ~/bin/nvim.appimage
wget https://github.com/neovim/neovim/releases/download/nightly/nvim.appimage -P ~/bin
chmod u+x ~/bin/nvim.appimage
