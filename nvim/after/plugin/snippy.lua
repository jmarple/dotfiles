local module_checker = require('module_checker')
if not module_checker.IsModuleAvailable('snippy') then
  return
end

-- require('snippy').setup {
--     mappings = {
--         is = {
--             ['<Tab>'] = 'expand_or_advance',
--             ['<S-Tab>'] = 'previous',
--         },
--         nx = {
--             ['<leader>x'] = 'cut_text',
--         },
--     },
-- }
