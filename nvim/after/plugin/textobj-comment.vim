let g:textobj_comment_no_default_key_mappings = 1
xmap ax <Plug>(textobj-comment-a)
omap ax <Plug>(textobj-comment-a)
xmap ix <Plug>(textobj-comment-i)
omap ix <Plug>(textobj-comment-i)
xmap aX <Plug>(textobj-big-a)
omap aX <Plug>(textobj-big-a)
xmap iX <Plug>(textobj-big-i)
omap iX <Plug>(textobj-big-a)

