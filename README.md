Collection of dotfiles for:
- neovim
- kitty
- tmux
- zsh

To install, clone the repo into the home directory and run the install script:

~~~
cd
git clone https://gitlab.com/jmarple/dotfiles.git
cd dotfiles
./install.sh
~~~

This will symlink the above dotfiles to the appropriate location and install a number of applications. 
