# Uncomment this and comment at bottom to profile zsh load time
# zmodload zsh/zprof

# Download Znap, if it's not there yet.
[[ -f ~/dotfiles/zsh/snap/zsh-snap/znap.zsh ]] ||
    git clone --depth 1 -- \
        https://github.com/joshuaMarple/zsh-snap.git ~/dotfiles/zsh/snap/zsh-snap

# source spaceship before starting prompt to speed it up even more
source ~/dotfiles/zsh/snap/zsh-snap/znap.zsh
# source ~/dotfiles/zsh/spaceship.zsh
ZVM_CURSOR_STYLE_ENABLED=true
ZVM_LAZY_KEYBINDINGS=false
ZVM_INIT_MODE=sourcing

znap source https://gitlab.com/jmarple/refined
# source ~/projects/refined/refined.plugin.zsh

znap source Aloxaf/fzf-tab
znap source zsh-users/zsh-autosuggestions
znap source mafredri/zsh-async
znap source seletskiy/zsh-fuzzy-search-and-edit
znap source MenkeTechnologies/zsh-expand
# znap source Valiev/almostontop
znap source jeffreytse/zsh-vi-mode

autoload -U select-word-style
select-word-style bash

## Zsh Core Settings
bindkey -v

# Its really annoying when zsh tries to match * for files
setopt +o nomatch

### History settings
HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000
setopt HIST_IGNORE_ALL_DUPS  # do not put duplicated command into history list
setopt HIST_SAVE_NO_DUPS  # do not save duplicated command
setopt HIST_REDUCE_BLANKS  # remove unnecessary blanks
setopt INC_APPEND_HISTORY_TIME  # append command to history file immediately after execution
setopt EXTENDED_HISTORY  # record command start time

# Keybinding to edit command line
autoload -z edit-command-line 
zle -N edit-command-line
bindkey "^v" edit-command-line

# Autojump
# https://duganchen.ca/the-simplest-autojump-implementation-for-zsh/
autoload -Uz chpwd_recent_dirs cdr add-zsh-hook
add-zsh-hook chpwd chpwd_recent_dirs
zstyle ':chpwd:*' recent-dirs-default yes
zstyle ':completion:*' recent-dirs-insert always
alias j=cdr
setopt complete_in_word
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}' 'r:|=*' 'l:|=* r:|=*'

# no need for cd
setopt auto_cd

source ~/.bash_aliases

bindkey '^ ' autosuggest-execute

bindkey '^F' fzf-file-widget

### Configure ZSH
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="bg=bold,underline"
ZSH_AUTOSUGGEST_USE_ASYNC=true

bindkey '^P' history-beginning-search-backward
bindkey '^N' history-beginning-search-forward

stty -ixon
bindkey '^q' push-line-or-edit

# https://github.com/spaceship-prompt/spaceship-prompt/issues/91
bindkey "^?" backward-delete-char

eval "$(zoxide init zsh)"

export BAT_THEME="ansi-light"

source ~/dotfiles/zsh/scripts/*

# zprof


# Load Angular CLI autocompletion.
source <(ng completion script)

# Added by Amplify CLI binary installer
export PATH="$HOME/.amplify/bin:$PATH"
